import express from 'express';
import MongoDB from './database/MongoDB.js';
import articleRouter from './routes/articles.js';

const app = express();
const port = 3000;

MongoDB.connect();

//this middleware is needed to populate the req.body with the incoming json
app.use(express.json());

app.use('/', articleRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});