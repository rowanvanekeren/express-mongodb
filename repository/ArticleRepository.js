import _ from 'lodash';
import MongoDB from '../database/MongoDB.js';
import { v4 as uuidv4 } from 'uuid';

export default class ArticleRepository {
    dbName = 'test_database';
    dbCollection = 'test_collection';

    async getCollection() { 
        const db = MongoDB.getClient().db(this.dbName);
        return db.collection(this.dbCollection);
    }

    /**
     * 
     * @param {string} id 
     * @returns object
     */
    async find(id) {
        return await (await this.getCollection()).findOne({ uuid: id });
    }

    /**
     * 
     * @param {string} id 
     * @returns boolean
     */
    async exists(id) {
        return !!ArticleRepository.find(id);
    }

    /**
     * 
     * @returns list of objects
     */
    async get() {
        return await (await this.getCollection()).find({}).toArray();
    }

    /**
     * 
     * @param {object} article 
     */
    async create(article) {
        if(typeof article !== 'object') {
            throw new Error("Article must be an object");
        }

        if(article.id) {
            throw new Error("Article id must be null or undefined");
        }

        article.uuid = uuidv4();

        await (await this.getCollection()).insertOne(article);
    }

    /**
     * 
     * @param {string} uuid 
     * @param {object} article 
     */
    async update(uuid, article) {
        if(typeof article !== 'object') {
            throw new Error("Article must be an object");
        }

        if(!uuid) {
            throw new Error("Article id not defined");
        }

        await (await this.getCollection()).updateOne({ uuid }, { $set: _.omit(article, ['uuid', '_id', 'id']) });
    }

    /**
     * 
     * @param {string} id 
     */
    async delete(id) {
        if(!id) {
            throw new Error("Article id not defined");
        }

        await (await this.getCollection()).deleteMany({ uuid: id });
    }
}