import express from 'express';
import ArticleController from '../controllers/ArticleController.js';

const router = express.Router();

router.get('/articles', ArticleController.getArticles);
router.get('/articles/:id', ArticleController.getArticle);
router.post('/articles', ArticleController.postArticle);
router.put('/articles/:id', ArticleController.putArticle);
router.delete('/articles/:id', ArticleController.deleteArticle);

export default router;