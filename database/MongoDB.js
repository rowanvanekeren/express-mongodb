
import { MongoClient } from 'mongodb';

export default class MongoDB {
    static dbClient;
    //root and example username and password, this is for testing purposes,
    //never use this in a production environment and switch to secure env variables
    static dbUrl = 'mongodb://root:example@localhost:27017';

    static async connect() {
        try{
            this.dbClient = new MongoClient(this.dbUrl);
            await this.dbClient.connect();
            console.log('Connected successfully to mongo server');
        }catch(e) {
            console.error(e);
            console.error('error connecting to mongo server, stopping script');
        }
    }

    static getClient() {
        return this.dbClient;
    }
}