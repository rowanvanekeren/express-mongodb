import ArticleRepository from "../repository/ArticleRepository.js";

export default class ArticleController {
    static async getArticle(req, res) {
        const articleRepository = new ArticleRepository();

        res.json(await articleRepository.find(req.params.id));
    }

    static async getArticles(req, res) {
        const articleRepository = new ArticleRepository();

        res.json(await articleRepository.get());
    }

    static async postArticle(req, res) {
        const articleRepository = new ArticleRepository();

        await articleRepository.create(req.body);

        res.json(await articleRepository.get());
    }

    static async putArticle(req, res) {
        const articleRepository = new ArticleRepository();

        const dbArticle = await articleRepository.find(req.params.id);
        
        if(dbArticle) {
            await articleRepository.update(dbArticle.uuid, req.body);
        }
        
        res.json(await articleRepository.get());
    }


    static async deleteArticle(req, res) {
        const articleRepository = new ArticleRepository();

        await articleRepository.delete(req.params.id);
        
        res.json(await articleRepository.get());
    }
}